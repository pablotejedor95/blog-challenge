import React from 'react';
import { Formik, Field, Form } from 'formik';
import axios from 'axios';
const Create = ({ history }) => {
  const handleSubmit = async () => {
    try {
      const response = await axios({
        method: 'post',
        url: 'https://jsonplaceholder.typicode.com/posts/',
      });
      console.log(response);
      history.replace('/');
    } catch {
      alert('Error');
    }
  };
  const handleReturn = () => {
    history.replace('/');
  };

  return (
    <div>
      <h1>Crear Post:</h1>
      <Formik
        onSubmit={handleSubmit}
        validate={values => {
          const errors = {};
          if (!values.title) {
            errors.title = 'Campo obligatorio';
          }

          if (!values.body) {
            errors.body = 'Campo obligatorio';
          }
          return errors;
        }}
        initialValues={{
          title: '',
          body: '',
        }}
      >
        {({ errors, isValid, touched }) => (
          <Form>
            <div className="m-3">
              <label htmlFor="inputTitle" className="form-label">
                Título:
              </label>
              <Field
                type="text"
                className="form-control"
                id="inputTitle"
                name="title"
              />
              {errors.title && touched.title ? (
                <div className="text-danger">{errors.title}</div>
              ) : null}
            </div>
            <div className="m-3">
              <label htmlFor="inputBody" className="form-label">
                Contenido:
              </label>
              <Field
                type="textarea"
                className="form-control"
                id="inputBody"
                name="body"
              />
              {errors.body && touched.body ? (
                <div className="text-danger">{errors.body}</div>
              ) : null}
            </div>
            <button className="btn btn-primary" onClick={handleReturn}>
              Volver
            </button>
            <button
              type="submit"
              className="btn btn-primary m-3"
              disabled={!isValid}
            >
              Enviar
            </button>
          </Form>
        )}
      </Formik>
    </div>
  );
};

export default Create;
