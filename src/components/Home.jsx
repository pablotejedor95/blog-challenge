import React, { useContext } from 'react';

import BlogEntry from './BlogEntry';
import Context from './Context';
const Home = () => {
  const context = useContext(Context);

  return (
    <div className="container m-3">
      <h1>Blog</h1>
      <h3 className="m-5">Entradas:</h3>
      {!context ? (
        <p className="text-danger">Cargando...</p>
      ) : (
        context.map(post => <BlogEntry entry={post} key={post.id} />)
      )}
    </div>
  );
};

export default Home;
