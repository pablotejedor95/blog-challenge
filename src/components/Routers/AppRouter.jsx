import React, { useEffect, useState } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Login from '../login/Login';
import Home from '../Home';
import ProtectedRoute from './ProtectedRoute';
import Edit from '../Edit';
import Create from '../Create';
import NavBar from '../ui/NavBar';
import axios from 'axios';
import Context from '../Context';
export const AppRouter = () => {
  const [posts, setPosts] = useState(null);
  const getPosts = async () => {
    try {
      const response = await axios.get(
        'https://jsonplaceholder.typicode.com/posts'
      );
      setPosts(response.data);
      return response;
    } catch {
      console.log('Error');
    }
  };

  useEffect(() => {
    getPosts();
  }, []);

  return (
    <Context.Provider value={posts}>
      <Router>
        <NavBar />
        <Switch>
          <Route exact path="/login" component={Login} />
          <ProtectedRoute exact path="/" component={Home} />
          <Route exact path="/edit/:entryId" component={Edit} />
          <Route exact path="/create" component={Create} />
        </Switch>
      </Router>
    </Context.Provider>
  );
};
