import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
const BlogEntry = ({ entry }) => {
  const [detailSwitcher, setDetailSwitcher] = useState(false);
  const showDetailsHandler = () => {
    setDetailSwitcher(!detailSwitcher);
  };
  const deleteHandler = async () => {
    await axios({
      method: 'delete',
      url: `https://jsonplaceholder.typicode.com/posts/${entry.id}`,
    });
  };
  return (
    <div className="container d-flex flex-column bg-light border rounded m-1 p-3">
      <h3>{entry.title}</h3>
      {detailSwitcher ? <p>{entry.body}</p> : null}
      <div className="d-flex">
        <button className="btn btn-secondary m-2" onClick={showDetailsHandler}>
          Detalles
        </button>
        <Link to={`./edit/${entry.id}`} className="btn btn-secondary m-2">
          Editar post
        </Link>
        <button className="btn btn-danger m-2" onClick={deleteHandler}>Borrar post</button>
      </div>
    </div>
  );
};

export default BlogEntry;
