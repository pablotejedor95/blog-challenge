import React from 'react';
import { Link } from 'react-router-dom';

const NavBar = () => {
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light border rounded">
      <div className="container-fluid">
        <div className="navbar-nav">
          <Link to="/" className="nav-link active" aria-current="page">
            Home
          </Link>
          <Link to="/create" className="nav-link">
            Crear
          </Link>
        </div>
      </div>
    </nav>
  );
};

export default NavBar;
